package com.twuc.webApp.domain;

import com.twuc.webApp.contract.CreateProductRequest;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @Size(max=64)
    @NotNull
    private
    String name;

    @Column
    @NotNull
    @Min(value = 1)
    @Max(value = 10000)
    private
    Integer price;

    @Column
    //@Size(max=32)
    @Length(max=32)
    @NotNull
    //@Column(nullable = false, length = 32)
    private
    String unit;

    public Product(String name, Integer price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Integer getPrice() {
        return this.price;
    }

    public String getUnit() {
        return this.unit;
    }

    public Product(CreateProductRequest request){
        this.unit = request.getUnit();
        this.price = request.getPrice();
        this.name = request.getName();
    }

}