package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/api/products")
    public ResponseEntity<String> createProduct(@Valid @RequestBody CreateProductRequest request){
        Product product = productRepository.save(new Product(request));
        productRepository.flush();
        return ResponseEntity.status(201).header("location","http://localhost/api/products/"+product.getId()).build();
    }

    @GetMapping("/api/products/{id}")
    public ResponseEntity<GetProductResponse> getProduct(@PathVariable Long id){
        Optional<Product> product = productRepository.findById(id);
        if(product.isEmpty()){
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).body(new GetProductResponse(product.get()));
    }
}